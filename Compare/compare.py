def compare(list1,list2):
    list3=[]
    for i in range(0,len(list1)-1):
        for j in range(0,len(list2)):
            if(list1[i]==list2[j]):
                list3.append(list2[j])
                i=i+1
    print("The common elements are \n")
    print(list3)
    
def common(a, b): 
    a_set = set(a) 
    b_set = set(b) 
    if (a_set & b_set): 
        print(a_set & b_set) 
    else: 
        print("No common elements") 


def main():
    lst1=[]
    n1=int(input("Enter Number of elements in List1 \n"))
    
    for i in range(0,n1):
        ele1=int(input())
        lst1.append(ele1)
    
    lst2=[]
    n2=int(input("Enter the number of elements in List2 \n"))
    
    for i in range(0,n2):
        ele2=int(input())
        lst2.append(ele2)
    
    compare(lst1,lst2)
    common(lst1,lst2)

main()