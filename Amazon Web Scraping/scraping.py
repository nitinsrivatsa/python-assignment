import requests
from bs4 import BeautifulSoup
import pandas as pd

products=[]
prices=[]
ratings=[]

keyword=input("Enter the Keyword You want to extract data of?")
url="https://www.amazon.in/s?k="+keyword

headers = {"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64;x64; rv:66.0) Gecko/20100101 Firefox/66.0", "Accept-Encoding":"gzip, deflate",     "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8", "DNT":"1","Connection":"close", "Upgrade-Insecure-Requests":"1"}
r = requests.get(url,headers=headers)
content = r.content
soup = BeautifulSoup(content)
for d in soup.findAll('div', attrs={'class':'sg-col-4-of-12 sg-col-8-of-16 sg-col-16-of-24 sg-col-12-of-20 sg-col-24-of-32 sg-col sg-col-28-of-36 sg-col-20-of-28'}):
    name = d.find('span', attrs={'class':'a-size-medium a-color-base a-text-normal'})
    price = d.find('span', attrs={'class':'a-offscreen'})
    rating = d.find('span', attrs={'class':'a-icon-alt'})
    all=[]
    if all is not None:
        products.append(name.text)
    else:
        products.append("unknown-product")
    if price is not None:
        prices.append(price.text)
    else:
        prices.append('0')
    if rating is not None:
        #print(rating.text)
        ratings.append(rating.text)
    else:
        ratings.append('-1')
    
df = pd.DataFrame({'Product Name':products,'Price':prices,'Rating':ratings})
df.to_csv('products100.csv')