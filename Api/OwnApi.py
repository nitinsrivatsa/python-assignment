from flask import Flask
from flask_restful import Api, Resource, reqparse
import random
app = Flask(__name__)
api = Api(app)

player_details = [
    {
        "id": 0,
        "player": "Cristiano Ronaldo",
        "details": "Regarded as one of the best and is known for his work ethic and dedication on the pitch " +
                 "Has 5 champions league and 5 Ballon d'ors" 
    },
    {
        "id": 1,
        "player": "Lionel Messi",
        "details": "The little magician is known for his creativity and magic on the field" +
                 "His rivalry with CR7 is the most awesome thing for the football fans"
    },
    {
        "id": 2,
        "player": "Zidane",
        "details": "The player who could turn any game in his favor by his technicality " +
                 "Everybody still remembers his performance again brazil"
    }
]

class Details(Resource):
    
    def get(self,id=0):
        if id==0:
            return random.choice(player_details),200
        
        for details in player_details:
            if(details["id"]==id):
                return detail,200
        return "Quote not found",404

    
#     def post(self, id):
#         parser = reqparse.RequestParser()
#         parser.add_argument("player")
#         parser.add_argument("details")
#         params = parser.parse_args()

#         for detail in player_details:
#             if(id == detail["id"]):
#                 return f"Quote with id {id} already exists", 400

#         detail = {
#           "id": int(id),
#           "player": params["player"],
#           "details": params["details"]
#       }

#         player_details.append(detail)
#         return quote, 201
    
api.add_resource(Details, "/player_details", "/player_details/", "/player_details/<int:id>")

if __name__ == '__main__':
    app.run(port='5007')