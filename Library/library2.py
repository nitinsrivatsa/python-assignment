import sys

class Login:
    def checklibrarian(self):
        window=Window()
        file1=open("myfile11.txt",'r')
        usrname=input("Enter Librarian Username \n")
        psswd=input("Enter Librarian Password \n")
        for line in file1:
            username,password=line.split()
            if((usrname==username)and(psswd==password)):
                print("LIBRARIAN ACCESS")
                window.windows()
            else:
                print("Nope Try Again")
                login.checklibrarian()
    
    def checkuser(self):
        file1=open("myfile15.txt",'r')
        usrname=input("Enter Username \n")
        psswd=input("Enter Password \n")
        window=Window()
        for line in file1:
            username,password=line.split()
            if((usrname in username)and(psswd in password)):
                window.windows()
            else:
                print("Wrong Attempt")
                
                
    def newuser(self):
        file1=open("myfile15.txt",'a')
        usrname=input("Enter Username")
        passwd=input("Enter Password")
        file1.write("".join(usrname))
        login=Login()
        l=' '
        file1.write(l)
        file1.write(passwd)
        l1='\n'
        file1.write(l1)
        file1.close()
        login.checkuser()
        
class Window:
    def windows(self):
        library=Library(["Star Wars","Planet of the Apes","Wall Street"])
        student=Student()
        while True:
            print("1. Display all available books"+"\n"+"2. Request a book"+"\n"+"3. Return a book"+"\n"+"4. Exit")
            choice=int(input("Enter Choice:"))
            if choice==1:
                library.displayAvailablebooks()
            elif choice==2:
                library.lendBook(student.requestBook())
            elif choice==3:
                library.addBook(student.returnBook())
            elif choice==4:
                sys.exit()
    
    
class Library:
    def __init__(self,listofbooks):
        self.allbooks=listofbooks

    def displayAvailablebooks(self):
        print("The books we have in our library are as follows:")
        for book in self.allbooks:
            print(book)
    def lendBook(self,requestedBook):
        if requestedBook in self.allbooks:
            print("The book you requested has now been borrowed")
            self.allbooks.remove(requestedBook)
        else:
            print("Sorry the book you have requested is currently not in the library")
                  
    def addBook(self,returnedBook):
        self.allbooks.append(returnedBook)
        print("Thanks for returning your borrowed book")
            

class Student:
    def requestBook(self):
        print("Enter the name of the book you'd like to borrow>>")
        self.book=input()
        return self.book

    def returnBook(self):
        print("Enter the name of the book you'd like to return>>")
        self.book=input()
        return self.book

def main():            
    login=Login()
    done=False
    while True:
        print("1. Login as Admin "+"\n"+"2. Login as User"+"\n"+"3.Register as New User")
        choice=int(input("Enter Your Choice"))
        if choice==1:
            login.checklibrarian()
        elif choice==2:
            login.checkuser()
        elif choice==3:
            login.newuser()
        elif choice==4:
            sys.exit()
                  
if __name__=='__main__':
    main()  