import pandas as pd
import csv
import sys

def Library():
    print("For Admin Press 1")
    print("For User Press 2")
    print("For New User Press 3")
    print("To Exit Press Something Else")
    user=int(input())
    if(user==1):
        checklibrarian()
    elif(user==2):
        checkuser()
    elif(user==3):
        newuser()
    else:
        sys.exit()
        
def checklibrarian():
    file1=open("myfile11.txt",'r')
    usrname=input("Enter Librarian Username \n")
    psswd=input("Enter Librarian Password \n")
    for line in file1:
        username,password=line.split()
        if((usrname==username)and(psswd==password)):
            librarian()
    print("Nope Try Again")
    checklibrarian()
    file1.close()
        
def librarian():
    print("To Add Books Press 1")
    print("To View Books Press 2")
    librarian_input=int(input("Enter The Operation to be Performed"))
    if(librarian_input==1):
        add_books()
    elif(librarian_input==2):
        view_books()
    else:
        print("Enter Correctly")
        librarian()

def user():
    print("To View Books Press 1"+"\n"+"To Borrow Books Press 2"+"\n"+"To Return Books Press 3")
    cuser_input=int(input())
    if(cuser_input==1):
        borrowbooks()
    elif(cuser_input==2):
        view_books()
    elif(cuser_input==3):
        return_books()
    else:
        print("Enter Correctly")
        user()

def checkuser():
    file1=open("myfile1.txt",'r')
    usrname=input("Enter Username \n")
    psswd=input("Enter Password \n")
    for line in file1:
        username,password=line.split()
        if((usrname==username)and(psswd==password)):
            user()
    print("Incorrect Username or Password")
    file1.close()
    
def newuser():
    file1=open("myfile1.txt",'a')
    usrname=input("Enter Username")
    passwd=input("Enter Password")
    file1.write("".join(usrname))
    l=' '
    file1.write(l)
    file1.write(passwd)
    l1='\n'
    file1.write(l1)
    file1.close()
    checkuser()
    
def add_books():
    file1=open('mybook123.csv','a',newline='')
    print("Enter Book_Id and Book_Name")
    book_id=int(input())
    with file1:      
        header=["Book_Id","Book_Name"]
        df=pd.read_csv("mybook123.csv",names=header)
        for i in range(len(df["Book_Id"])):
            if(book_id==df["Book_Id"][i]):
                print("Already Exists")
                break
        book_name=input()
        writer=csv.DictWriter(file1,fieldnames=header)
        writer.writerow({'Book_Id':book_id,'Book_Name':book_name})

    main()

def view_books():
    header=["Book_Id","Book_Name"]
    df=pd.read_csv("mybook123.csv",names=header)
    print(df)
    main()
    
def borrowbooks():
    header=["Book_Id","Book_Name"]
    df=pd.read_csv("mybook123.csv",names=header)
    print("List of Books Available with Book_Id is")
    print(df)
    book_id=int(input("Enter Book_id"))
    for i in range(len(df["Book_Id"])):
        if(book_id==df["Book_Id"][i]):
            print("You Just Borrowed " + df["Book_Name"][i])
        else:
            print("Enter Correct ID")
    main()
    
def return_books():
    header=["Book_Id","Book_Name"]
    df=pd.read_csv("mybook123.csv",names=header)
    book_id=int(input("Enter Book_id of the book you want to return"))
    for i in range(len(df["Book_Id"])):
        if(book_id==df["Book_Id"][i]):
            print("Thank you for returning" + "\t" + df["Book_Name"][i])
    if(i==(len(df["Book_Id"])-1)):
        print("InCorrect Book Id")
    main()
    
def main():
    print("Welcome to Library Management System")
    print("Let's go to login/register Page")
    Library()
    
if __name__=='__main__':
    main()